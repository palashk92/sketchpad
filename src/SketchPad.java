import sun.awt.image.codec.JPEGImageEncoderImpl;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class SketchPad extends Frame {
    public class Obj {
        int xi,yi,xj,yj,tp,sl=0,cl, id;
        Obj() { }

        Obj(int a, int b, int c, int d, int t, int col) {
            xi=a; yi=b; xj=c; yj=d; tp=t; cl=col;}

        class ipair {
            int x,y;
            ipair(int xx, int yy) { x=xx; y=yy; }
        }

        int dist(ipair P, ipair Q) { return (int)Math.sqrt((P.x-Q.x)*(P.x-Q.x) + (P.y-Q.y)*(P.y-Q.y)); }
        int segdist(int xp,int yp) { // distance from point to line segment (initial diagonal pair)
            ipair I=new ipair(xi,yi), J=new ipair(xj,yj), P=new ipair(xp,yp);
            return dist(I,P)>dist(J,P)? dist(J,P):dist(I,P);
        }
    }
    int type=100,dmin=999999,select=0,x0,y0,xp,yp,x01,y01,click=0,complete=0,color=0, check = 0;

    ArrayList<Obj> objs= new ArrayList<Obj>();
    ArrayList<Obj> obju = new ArrayList<Obj>();
    Obj closest = new Obj();

    SketchPad(){
        setLayout(new FlowLayout(FlowLayout.TRAILING));

        //Button Container
        Panel ButtonPalette = new Panel();
        ButtonPalette.setLayout(new GridLayout(15, 1));
        this.add(ButtonPalette);

        Panel ColorPalette = new Panel();
        ColorPalette.setLayout(new GridLayout(1, 3));

        //Shapes Buttons
        Button btn1 = new Button("Line");
        ButtonPalette.add(btn1);
        btn1.addActionListener(e -> type=0);

        Button btn2 = new Button("Rectangle");
        ButtonPalette.add(btn2);
        btn2.addActionListener(e -> type=1);

        Button btn3 = new Button("Square");
        ButtonPalette.add(btn3);
        btn3.addActionListener(e -> type=2);

        Button btn4 = new Button("Circle");
        ButtonPalette.add(btn4);
        btn4.addActionListener(e -> type=3);

        Button btn5 = new Button("Ellipse");
        ButtonPalette.add(btn5);
        btn5.addActionListener(e -> type=4);

        Button btn6 = new Button("Free Hand");
        ButtonPalette.add(btn6);
        btn6.addActionListener(e -> type=5);

        Button btn7 = new Button("Open Polygon");
        ButtonPalette.add(btn7);
        btn7.addActionListener(e -> type=6);

        Button btn8 = new Button("Closed Polygon");
        ButtonPalette.add(btn8);
        btn8.addActionListener(e -> type=7);

        //Clear Button
        Button btn9 = new Button("Clear");
        ButtonPalette.add(btn9);
        btn9.addActionListener(e -> {objs.clear();repaint();});

        Button btn14 = new Button("Select");
        ButtonPalette.add(btn14);
        btn14.addActionListener(e -> {
            select = 1;
            closest.sl = 0;
            repaint();
        });

        Button btn15 = new Button("Undo");
        ButtonPalette.add(btn15);
        btn15.addActionListener(e -> {
            if(objs.size()!= 0) {
                obju.add(objs.get(objs.size() - 1));
                System.out.println(obju);
                objs.remove(objs.size() - 1);
            }
            repaint();
        });

        Button btn16 = new Button("Redo");
        ButtonPalette.add(btn16);
        btn16.addActionListener(e -> {
            if(obju.size() != 0) {
                objs.add(obju.get(obju.size() - 1));
                System.out.println(objs.get(objs.size() - 1));
                obju.remove(obju.size() - 1);
            }
            repaint();
        });

        Button btn17 = new Button("Cut");
        ButtonPalette.add(btn17);
        btn17.addActionListener(e -> {
            select = 1;
            check = 2;
        });

        Button btn18 = new Button("Paste");
        ButtonPalette.add(btn18);
        btn18.addActionListener(e -> {
            select = 1;
            check = 3;
        });

        //Color Buttons Palette
        ButtonPalette.add(ColorPalette);
        Button btn10 = new Button("Black");
        ColorPalette.add(btn10);
        btn10.addActionListener(e -> color=0);

        Button btn11 = new Button("Red");
        ColorPalette.add(btn11);
        btn11.addActionListener(e -> color=1);

        Button btn12 = new Button("Blue");
        ColorPalette.add(btn12);
        btn12.addActionListener(e -> color=2);


        addMouseListener(new MouseAdapter()   {
            public void mousePressed(MouseEvent e){
                if(select == 1 && check ==3) {
                    if(objs.size() != 0) {
                        int index = objs.indexOf(obju.get(obju.size() - 1));
                        System.out.println(index);
                        objs.add(obju.get(index));
                        obju.remove(index);
                        objs.get(index).cl = 0;
                    }
                }

                if(select == 1 && check == 4){
                    int index = objs.indexOf(obju.get(obju.size() - 1));
                }
                if(type==6 && e.getButton()==3) {click=1;}

                else if(type==7 && e.getButton()==3) {
                    click=1;
                    x0=e.getX();
                    y0=e.getY();
                    objs.add(new Obj(x01,y01,x0,y0,type,color));
                    complete=1;
                    repaint();
                }

                else if(type==7 && e.getButton()==1){click=0;
                    if(objs.size()==0) {x01=e.getX();y01=e.getY();}
                    else if(complete==1) {x01=e.getX();y01=e.getY();complete=0;}
                    x0=e.getX();y0=e.getY();
                    objs.add(new Obj(x0,y0,x0,y0,type,color));}

                else {
                    x0=e.getX();y0=e.getY();
                    xp=x0;yp=y0;
                    objs.add(new Obj(x0,y0,x0,y0,type,color));
                    click=0;
                }}

            public void mouseReleased(MouseEvent e){
                if(select == 1 && check ==2) {
                    if(obju.size() != 0) {
                        int index = objs.indexOf(obju.get(obju.size() - 1));
                        System.out.println(index);
                        obju.add(objs.get(index));
                        objs.remove(index);
                    }
                }
                select=0;
                check = 0;
            }
        });

        //Mouse Motion Listener
        addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseMoved(MouseEvent e) {
                if (select==1 && objs.size()!=0) {//nearest object
                    objs.forEach(ob ->{
                        ob.sl = 0;
                        int d=ob.segdist(e.getX(),e.getY());
                        if( dmin > d ) {
                            closest=ob;
                            dmin=d;
                        }});
                    closest.sl=1;
                    repaint();
                }
                if ((type==6||type==7) && objs.size()!=0 && click==0) {//Polygon
                    if(objs.get(objs.size()-1).tp==6 || objs.get(objs.size()-1).tp==7) {
                        objs.remove( objs.size()-1 );
                        objs.add(new Obj(x0,y0,e.getX(),e.getY(),type,color));
                        repaint();
                    }}
            }//End of Mouse Moved

            public void mouseDragged(MouseEvent e){
                //For Free Hand Drawing
                if(type==5) {
                    objs.add(new Obj(xp,yp,e.getX(),e.getY(),type,color));
                    xp=e.getX(); yp=e.getY();
                    repaint();}
                else {
                    objs.remove( objs.size()-1 );
                    objs.add(new Obj(x0,y0,e.getX(),e.getY(),type,color));
                    repaint();
                }
            }
        });

        //For Closing the Frame
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent w) {
                dispose();
            }});
    }

    int max(int a,int b){return a>b ? a:b;}
    int min(int a,int b){return a>b ? b:a;}
    int abs(int a){return a>0 ? a : -a;}

    public void paint(Graphics g){
        dmin=9999999;
        objs.forEach(ob -> {
            int x = min(ob.xi,ob.xj);
            int y = min(ob.yi,ob.yj);
            if (ob.sl == 1 ) {
                g.setColor(Color.YELLOW);
                if(check == 2) {
                    obju.add(ob);
                }
            }
            else if(ob.sl == 2) {
                g.setColor(Color.YELLOW);
                repaint();
            }
            else if (ob.cl == 0)
                g.setColor(Color.BLACK);
            else if (ob.cl == 1)
                g.setColor(Color.RED);
            else if (ob.cl == 2)
                g.setColor(Color.BLUE);
            if (ob.tp == 0)
                g.drawLine(ob.xi,ob.yi,ob.xj,ob.yj);
            if (ob.tp == 1)
                g.drawRect(x,y,abs(ob.xi-ob.xj),abs(ob.yi-ob.yj));
            if (ob.tp == 2)
                g.drawRect(x,y,abs(ob.xi-ob.xj),abs(ob.xi-ob.xj));
            if (ob.tp == 3)
                g.drawOval(x,y,abs(ob.xi-ob.xj),abs(ob.xi-ob.xj));
            if (ob.tp == 4)
                g.drawOval(x,y,abs(ob.xi-ob.xj),abs(ob.yi - ob.yj));
            if (ob.tp == 5 || ob.tp == 6 || ob.tp == 7)
                g.drawLine(ob.xi,ob.yi,ob.xj,ob.yj);
        });}

    public static void main(String[] args)  {
        SketchPad sketch = new SketchPad();
        sketch.setSize(500, 500);
        sketch.setVisible(true);
    }

}